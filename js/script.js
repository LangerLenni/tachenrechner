let currentInput = '';

//write a number into the Display
function appendNumber(number) {
    currentInput += number;
    updateDisplay();
}

//write an operator into the Display
function addOperator(operator) {
    currentInput += operator;
    updateDisplay();
}

//calculate the input
function calculate(){
    currentInput = eval(currentInput);
    updateDisplay();
}

//clear the Display
function clearDisplay(){
    currentInput = '';
    updateDisplay();
}

//updates the Display with the currentInput
function updateDisplay() {
    document.getElementById('display').value = currentInput;
}

//eventlistener for the input
document.addEventListener('keydown', function (event) {
    // Check if the pressed key is '0' on the numpad
    if (event.key === '0') {
        appendNumber('0');
    }
    if (event.key === '1') {
        appendNumber('1');
    }
    if (event.key === '2') {
        appendNumber('2');
    }
    if (event.key === '3') {
        appendNumber('3');
    }
    if (event.key === '4') {
        appendNumber('4');
    }
    if (event.key === '5') {
        appendNumber('5');
    }
    if (event.key === '6') {
        appendNumber('6');
    }
    if (event.key === '7') {
        appendNumber('7');
    }
    if (event.key === '8') {
        appendNumber('8');
    }
    if (event.key === '9') {
        appendNumber('9');
    }
    if (event.key === '+') {
        appendNumber('+');
    }
    if (event.key === '-') {
        appendNumber('-');
    }
    if (event.key === '*') {
        appendNumber('*');
    }
    if (event.key === '/') {
        appendNumber('/');
    }
    if (event.key === 'Enter') {
        calculate();
    }
    if (event.key === ',') {
        clearDisplay();
    }
  });